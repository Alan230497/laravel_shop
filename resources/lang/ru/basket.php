<?php

return [
    'you_order_confirmed' => 'Ваш заказ принят в обработку!',
    'not_available' => 'Товар не доступен для заказа в полном объеме',
    'basket_main' => ['basket' => 'Корзина', 'order_registration' => 'Оформление заказа', 'name' => 'Имя',
    'number' => 'Количество', 'price' => 'Цена', 'cost' => 'Стоимость', 'total_cost' => 'Общая стоимость',
    'place_an_order' => 'Оформить заказ', 'product_added' => 'Добавлен товар', 'product_remove' => 'Удален товар',
     'product_not_available_more' => 'в большем количестве не доступен'],
];
