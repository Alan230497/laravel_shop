<?php

return [
    'admin_panel' => 'Панель администратора', 'name' => 'Название', 'code' => 'Код', 'edit' => 'Редактировать', 'delete' => 'Удалить',
    'admin_title' => 'Админка', 'add_category' => 'Добавить категорию', 'add_product' => 'Добавить товар', 'add_property' => 'Добавить свойство',
    'title' => ['orders_title' => 'Заказы', 'categories_title' => 'Категории', 'products_title' => 'Товары', 'properties_title' => 'Свойства',
        'form_edit_categories' => 'Редактировать категорию', 'form_create_categories' => 'Создать категорию', 'form_edit_property' => 'Редактировать свойство',
        'form_create_property' => 'Создать свойство', 'form_edit_products' => 'Редактировать товар', 'form_create_products' => 'Создать товар',
        'form_edit_property_variant' => 'Редактировать вариант свойства',
        'show_products' => 'Товар', 'show_categories' => 'Категория', 'show_property' => 'Свойство', 'property_options' => 'Варианты свойства',
        'show_property_options_property_variant' => 'Вариант свойства'],
    'products' => ['products' => 'Товары', 'category' => 'Категория'],
    'category' => 'Категория', 'admin_property' => 'Свойство', 'add_property_variant' => 'Добавить вариант свойства', 'form_save' => 'Сохранить',
    'number_of_product_offers' => 'Кол-во товарных предложений', 'product_offers' => 'Товарные предложения', 'add_sku' => 'Добавить Sku', 'edit_sku' => 'Редактировать Sku',
    'product_offer' => 'Товарное предложение (свойства)', 'value' => 'Значение', 'field' => 'Поле',
];
