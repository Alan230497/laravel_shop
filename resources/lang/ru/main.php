<?php

return [
    'online_shop' => 'Интернет Магазин', 'all_products' => 'Все товары', 'categories' => 'Категории',
    'your_cart' => 'В корзину', 'reset_project_to_default_state' => 'Сбросить проект в начальное состояние',
    'login' => 'Войти', 'register' => 'Регистрация',
    'logout' => 'Выйти',
    'price_from' => 'Цена от', 'price_to' => 'до',
    'filter_products' => 'Фильтр', 'reset_filter' => 'Сброс',
    'my_orders' => 'Мои заказы',
    'title' => ['main_title' => 'Главная', 'categories_title' => 'Категории', 'your_cart_title' => 'Корзина'],
    'back_to_site' => 'Вернуться на сайт',
    'filter_labels' => ['new' => 'Новинка', 'hit' => 'Хит продаж!', 'recommend' => 'Рекомендуем'],
    'not_available' => 'Не доступен',
    'more_details' => 'Подробнее',
    'current_lang' => 'ru',
    'set_lang' => 'en',
    'properties' => [
        'hit' => 'Хит',
        'recommend' => 'Рекомендуем',
        'new' => 'Новинка',
    ],
    'main.subscribe_product' => 'Спасибо, мы сообщим вам о поступлении товара',
];
