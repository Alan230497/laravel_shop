<?php
return [
    'you_order_confirmed' => 'Your order confirmed',
    'not_available' => 'The product is not available for order in full',
    'basket_main' => ['basket' => 'Basket', 'order_registration' => 'Order registration', 'name' => 'Name',
    'number' => 'Number', 'price' => 'Price', 'cost' => 'Cost', 'total_cost' => 'Total cost',
    'place_an_order' => 'Place an order', 'basket_added' => 'Product added', 'product_remove' => 'Product remove',
     'product_not_available_more' => 'Product not available more'],
];
