<?php

return [
  'confirm_order' => 'Confirm order', 'confirm_order_button' => 'Confirm order', 'open' => 'Open', 'order' => 'Order',
  'contact_you' => 'Enter your name and phone number so that our Manager can contact you', 'customer' => 'Customer',
  'phone_number' => 'Phone number', 'phone' => 'Phone', 'when_sent' => 'When sent', 'actions' => 'Actions',

];
