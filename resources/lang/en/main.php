<?php

return [
    'online_shop' => 'Online shop', 'all_products' => 'All products', 'categories' => 'Categories',
    'your_cart' => 'Your cart', 'reset_project_to_default_state' => 'Reset project to default state',
    'login' => 'Login', 'register' => 'Register',
    'logout' => 'Logout',
    'price_from' => 'Price From', 'price_to' => 'to',
    'filter_products' => 'Filter', 'reset_filter' => 'Reset',
    'my_orders' => 'My orders',
    'current_lang' => 'en',
    'set_lang' => 'ru',
    'title' => ['main_title' => 'Main', 'categories_title' => 'Categories', 'your_cart_title' => 'Basket'],
    'back_to_site' => 'Back to site',
    'filter_labels' => ['new' => 'New', 'hit' => 'Hit!', 'recommend' => 'Recommend'],
    'not_available' => 'Not available',
    'more_details' => 'More Details',
    'properties' => [
        'hit' => 'Hit',
        'recommend' => 'Recommend',
        'new' => 'New',
    ],
    'main.subscribe_product' => 'Thank you, we will notify you when the goods arrive',
];
