<?php

return [
    'admin_panel' => 'Admin panel', 'name' => 'Name', 'code' => 'Code', 'edit' => 'Edit', 'delete' => 'Delete',
    'admin_title' => 'Admin', 'add_category' => 'Add category', 'add_product' => 'Add product', 'add_property' => 'Add property',
    'title' => ['orders_title' => 'Orders', 'categories_title' => 'Categories', 'products_title' => 'Products', 'properties_title' => 'Properties',
        'form_edit_categories' => 'Edit category', 'form_create_categories' => 'Create category', 'form_edit_property' => 'Edit property',
        'form_create_property' => 'Create property', 'form_edit_products' => 'Edit products', 'form_create_products' => 'Create produtcs',
        'form_edit_property_variant' => 'Edit property variant',
        'show_products' => 'Product', 'show_categories' => 'Category', 'show_property' => 'Property', 'property_options' => 'Property options',
        'form_edit_property_options' => 'Edit property variant', 'show_property_options_property_variant' => 'Property variant'],
    'category' => 'Category', 'admin_property' => 'Property', 'add_property_variant' => 'Add property variant', 'form_save' => 'Save',
    'number_of_product_offers' => 'Number of product offers', 'product_offers' => 'Product offers', 'add_sku' => 'Add Sku', 'edit_sku' => 'Edit Sku',
    'product_offer' => 'Product offer (property)', 'value' => 'Value', 'field' => 'Field',
];
