@extends('layouts.app')

@section('title', __('admin.title.orders_title'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('admin.title.orders_title')</h1>
                <table class="table">
                    <tbody>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            @lang('basket.basket_main.name')
                        </th>
                        <th>
                            @lang('order.phone')
                        </th>
                        <th>
                            @lang('order.when_sent')
                        </th>
                        <th>
                            @lang('basket.basket_main.total_cost')
                        </th>
                        <th>
                            @lang('order.actions')
                        </th>
                    </tr>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->name }}</td>
                            <td>{{ $order->phone }}</td>
                            <td>{{ $order->created_at->format('H:i:s d/m/Y') }}</td>
                            <td>{{ $order->sum }} {{ $order->currency->symbol }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <a class="btn btn-success" type="button"
                                     @admin
                                       href="{{ route('orders.show', $order) }}">@lang('order.open')</a>
                                    @else
                                        href="{{ route('person.orders.show', $order) }}">@lang('order.open')</a>
                                    @endadmin
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{ $orders->links() }}
    </div>
@endsection
