@extends('layouts.app')

@section('title', __('admin.title.categories_title'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('admin.title.categories_title')</h1>
                <table class="table">
                    <tbody>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            @lang('admin.code')
                        </th>
                        <th>
                            @lang('admin.name')
                        </th>
                        <th>
                            @lang('order.actions')
                        </th>
                    </tr>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->code }}</td>
                            <td>{{ $category->__('name') }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <form action="{{ route('categories.destroy', $category) }}" method="POST">
                                        <a class="btn btn-success" type="button"
                                           href="{{ route('categories.show', $category) }}">@lang('order.open')</a>
                                        <a class="btn btn-warning" type="button"
                                           href="{{ route('categories.edit', $category) }}">@lang('admin.edit')</a>
                                        @csrf
                                        @method('DELETE')
                                        <input class="btn btn-danger" type="submit" value="@lang('admin.delete')"></form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                        {{ $categories->links() }}
                <a class="btn btn-success" type="button"
                   href="{{ route('categories.create') }}">@lang('admin.add_category')</a>
            </div>
        </div>
    </div>
@endsection
