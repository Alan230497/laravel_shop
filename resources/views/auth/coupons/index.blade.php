@extends('layouts.app')

@section('title', 'Купоны')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>Купоны</h1>
                <table class="table">
                    <tbody>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            @lang('admin.code')
                        </th>
                        <th>
                            Описание
                        </th>
                        <th>
                            @lang('order.actions')
                        </th>
                    </tr>
                    @foreach($coupons as $coupon)
                        <tr>
                            <td>{{ $coupon->id}}</td>
                            <td>{{ $coupon->code }}</td>
                            <td> {{ $coupon->description }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <form action="{{ route('coupons.destroy', $coupon) }}" method="POST">
                                        <a class="btn btn-success" type="button"
                                           href="{{ route('coupons.show', $coupon) }}">@lang('order.open')</a>
                                        <a class="btn btn-warning" type="button"
                                           href="{{ route('coupons.edit', $coupon) }}">@lang('admin.edit')</a>
                                        @csrf
                                        @method('DELETE')
                                        <input class="btn btn-danger" type="submit" value="@lang('admin.delete')"></form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $coupons->links() }}
                <a class="btn btn-success" type="button" href="{{ route('coupons.create') }}">Добавить купон</a>
            </div>
        </div>
    </div>
@endsection
