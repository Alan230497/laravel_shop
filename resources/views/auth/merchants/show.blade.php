@extends('layouts.app')

@section('title', 'Добавить поставщика' . $merchant->name)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Поставщик {{ $merchant->name }}</h1>
            <table class="table">
                <tbody>
                <tr>
                    <th>
                        Поле
                    </th>
                    <th>
                        Значение
                    </th>
                </tr>
                <tr>
                    <td>ID</td>
                    <td>{{ $merchant->id }}</td>
                </tr>
                <tr>
                    <td>Название</td>
                    <td>{{ $merchant->name }}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{{ $merchant->email }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        </div>
    </div>
@endsection
