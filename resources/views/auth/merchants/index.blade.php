@extends('layouts.app')

@section('title', 'Поставщики')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>Поставщики</h1>
                @if(session()->has('success'))
                    <p class="alert alert-success cc_cursor">{{ session()->get('success') }}</p>
                @endif
                <table class="table">
                    <tbody>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            @lang('admin.name')
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            @lang('order.actions')
                        </th>
                    </tr>
                    @foreach($merchants as $merchant)
                        <tr>
                            <td>{{ $merchant->id }}</td>
                            <td>{{ $merchant->name }}</td>
                            <td>{{ $merchant->email }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <form action="{{ route('merchants.destroy', $merchant) }}" method="POST">
                                        <a class="btn btn-success" type="button"
                                           href="{{ route('merchants.show', $merchant) }}">@lang('order.open')</a>
                                        <a class="btn btn-warning" type="button"
                                           href="{{ route('merchants.edit', $merchant) }}">@lang('admin.edit')</a>
                                        <a class="btn btn-dark" type="button"
                                           href="{{ route('merchants.update_token', $merchant) }}">Обновить токен</a>
                                        @csrf
                                        @method('DELETE')
                                        <input class="btn btn-danger" type="submit" value="@lang('admin.delete')"></form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                        {{ $merchants->links() }}
                <a class="btn btn-success" type="button"
                   href="{{ route('merchants.create') }}">Добавить поставщика</a>
            </div>
        </div>
    </div>
@endsection
