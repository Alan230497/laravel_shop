@extends('layouts.app')

@isset($merchant)
    @section('title', 'Редактировать поставщика '. $merchant->name)
@else
    @section('title', 'Создать поставщика')
@endisset

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @isset($merchant)
                    <h1>Редактировать поставщика <b>{{ $merchant->name }}</b></h1>
                @else
                    <h1>Создать поставщика</h1>
                @endisset

                <form method="POST" enctype="multipart/form-data"
                      @isset($merchant)
                      action="{{ route('merchants.update', $merchant) }}"
                      @else
                      action="{{ route('merchants.store') }}"
                    @endisset
                >
                    <div>
                        @isset($merchant)
                            @method('PUT')
                        @endisset
                        @csrf

                        <div class="input-group row">
                            <label for="name" class="col-sm-2 col-form-label">@lang('admin.name'): </label>
                            <div class="col-sm-6">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="text" class="form-control" name="name" id="name"
                                       value="{{ old('name', isset($merchant) ? $merchant->name : null) }}">
                            </div>
                        </div>
                        <br>

                            <div class="input-group row">
                                <label for="email" class="col-sm-2 col-form-label">Email: </label>
                                <div class="col-sm-6">
                                    @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <input type="text" class="form-control" name="email" id="email"
                                           value="{{ old('name', isset($merchant) ? $merchant->email : null) }}">
                                </div>
                            </div>
                            <br>

                        <button class="btn btn-success">@lang('admin.form_save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
