@extends('layouts.app')

@section('title', 'Sku ' . $sku->name)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Sku {{ $sku->product->name }}</h1>
            <h2>{{ $sku->propertyOptions->map->name->implode(', ') }}</h2>
            <table class="table">
                <tbody>
                <tr>
                    <th>
                        @lang('admin.field')
                    </th>
                    <th>
                        @lang('admin.value')
                    </th>
                </tr>
                <tr>
                    <td>ID</td>
                    <td>{{ $sku->id }}</td>
                </tr>
                <tr>
                    <td>@lang('basket.basket_main.price')</td>
                    <td>{{ $sku->price }}</td>
                </tr>
                <tr>
                    <td>@lang('basket.basket_main.number')</td>
                    <td>{{ $sku->count }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        </div>
    </div>
@endsection
