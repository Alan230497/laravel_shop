@extends('layouts.app')

@section('title', __('admin.product_offers'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('admin.product_offers')</h1>
                <h2>{{ $product->__('name') }}</h2>
                <table class="table">
                    <tbody>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            @lang('admin.product_offer')
                        </th>
                        <th>
                            @lang('order.actions')
                        </th>
                    </tr>

                    @foreach($skus as $sku)
                        <tr>
                            <td>{{ $sku->id }}</td>
                            <td>{{ $sku->propertyOptions->map->name->implode(', ') }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <form action="{{ route('skus.destroy', [$product, $sku]) }}" method="POST">
                                        <a class="btn btn-success" type="button"
                                           href="{{ route('skus.show', [$product, $sku]) }}">@lang('order.open')</a>
                                        <a class="btn btn-warning" type="button"
                                           href="{{ route('skus.edit', [$product, $sku]) }}">@lang('admin.edit')</a>
                                        @csrf
                                        @method('DELETE')
                                        <input class="btn btn-danger" type="submit" value="@lang('admin.delete')"></form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                        {{ $skus->links() }}
                <a class="btn btn-success" type="button"
                   href="{{ route('skus.create', $product) }}">@lang('admin.add_sku')</a>
            </div>
        </div>
    </div>
@endsection
