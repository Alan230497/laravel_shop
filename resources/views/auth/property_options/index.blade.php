@extends('layouts.app')

@section('title', __('admin.title.property_options'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('admin.title.property_options')</h1>
                <table class="table">
                    <tbody>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            @lang('admin.admin_property')
                        </th>
                        <th>
                            @lang('admin.name')
                        </th>
                        <th>
                            @lang('order.actions')
                        </th>
                    </tr>
                    @foreach($propertyOptions as $propertyOption)
                        <tr>
                            <td>{{ $propertyOption->id }}</td>
                            <td>{{ $propertyOption->property->__('name') }}</td>
                            <td>{{ $propertyOption->__('name') }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <form action="{{ route('property-options.destroy', [$property, $propertyOption]) }}" method="POST">
                                        <a class="btn btn-success" type="button"
                                           href="{{ route('property-options.show', [$property, $propertyOption]) }}">@lang('order.open')</a>
                                        <a class="btn btn-warning" type="button"
                                           href="{{ route('property-options.edit', [$property, $propertyOption]) }}">@lang('admin.edit')</a>
                                        @csrf
                                        @method('DELETE')
                                        <input class="btn btn-danger" type="submit" value="@lang('admin.delete')"></form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                        {{ $propertyOptions->links() }}
                <a class="btn btn-success" type="button"
                   href="{{ route('property-options.create', $property) }}">@lang('admin.add_property_variant')</a>
            </div>
        </div>
    </div>
@endsection
