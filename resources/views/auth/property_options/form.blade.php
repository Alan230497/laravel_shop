@extends('layouts.app')

@isset($propertyOption)
    @section('title', __('admin.title.form_edit_property_variant') . ' ' . $propertyOption->name)
@else
    @section('title', __('admin.add_property_variant'))
@endisset

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @isset($propertyOption)
                    <h1>@lang('admin.title.form_edit_property_variant') <b>{{ $propertyOption->name }}</b></h1>
                @else
                    <h1>@lang('admin.add_property_variant')</h1>
                @endisset

                <form method="POST" enctype="multipart/form-data"
                      @isset($propertyOption)
                      action="{{ route('property-options.update', [$property, $propertyOption]) }}"
                      @else
                      action="{{ route('property-options.store', $property) }}"
                    @endisset
                >
                    <div>
                        @isset($propertyOption)
                            @method('PUT')
                        @endisset
                        @csrf

                            <div>
                                <h2>Свойство {{ $property->name }}</h2>
                            </div>

                        <div class="input-group row">
                            <label for="name" class="col-sm-2 col-form-label">@lang('admin.name'): </label>
                            <div class="col-sm-6">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="text" class="form-control" name="name" id="name"
                                       value="{{ old('name', isset($propertyOption) ? $propertyOption->name : null) }}">
                            </div>
                        </div>
                        <br>

                        <div class="input-group row">
                            <label for="name" class="col-sm-2 col-form-label">@lang('admin.name') en: </label>
                            <div class="col-sm-6">
                                @error('name_en')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="text" class="form-control" name="name_en" id="name_en"
                                       value="{{ old('name', isset($propertyOption) ? $propertyOption->name_en : null) }}">
                            </div>
                        </div>
                        <br>

                        <button class="btn btn-success">@lang('admin.form_save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
