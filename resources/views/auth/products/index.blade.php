@extends('layouts.app')

@section('title', __('admin.title.products_title'))

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('admin.title.products_title')</h1>
                <table class="table">
                    <tbody>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            @lang('admin.code')
                        </th>
                        <th>
                            @lang('admin.name')
                        </th>
                        <th>
                            @lang('admin.category')
                        </th>
                        <th>
                            @lang('admin.number_of_product_offers')
                        </th>
                        <th>
                            @lang('order.actions')
                        </th>
                    </tr>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id}}</td>
                            <td>{{ $product->code }}</td>
                            <td>{{ $product->__('name') }}</td>
                            <td>{{ $product->category->__('name') }}</td>
                            <td></td>
                            <td>
                                <div class="btn-group" role="group">
                                    <form action="{{ route('products.destroy', $product) }}" method="POST">
                                        <a class="btn btn-success" type="button"
                                           href="{{ route('products.show', $product) }}">@lang('order.open')</a>
                                        <a class="btn btn-primary" type="button"
                                           href="{{ route('skus.index', $product) }}">Skus</a>
                                        <a class="btn btn-warning" type="button"
                                           href="{{ route('products.edit', $product) }}">@lang('admin.edit')</a>
                                        @csrf
                                        @method('DELETE')
                                        <input class="btn btn-danger" type="submit" value="@lang('admin.delete')"></form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $products->links() }}
                <a class="btn btn-success" type="button" href="{{ route('products.create') }}">@lang('admin.add_product')</a>
            </div>
        </div>
    </div>
@endsection
