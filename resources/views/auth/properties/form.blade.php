@extends('layouts.app')

@isset($property)
    @section('title', __('admin.title.form_edit_property') . ' ' . $property->name)
@else
    @section('title', __('admin.title.form_create_property'))
@endisset

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @isset($property)
                    <h1>@lang('admin.title.form_edit_property') <b>{{ $property->name }}</b></h1>
                @else
                    <h1>@lang('admin.title.form_create_property')</h1>
                @endisset

                <form method="POST" enctype="multipart/form-data"
                      @isset($property)
                      action="{{ route('properties.update', $property) }}"
                      @else
                      action="{{ route('properties.store') }}"
                    @endisset
                >
                    <div>
                        @isset($property)
                            @method('PUT')
                        @endisset
                        @csrf

                        <div class="input-group row">
                            <label for="name" class="col-sm-2 col-form-label">@lang('admin.name'): </label>
                            <div class="col-sm-6">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="text" class="form-control" name="name" id="name"
                                       value="{{ old('name', isset($property) ? $property->name : null) }}">
                            </div>
                        </div>
                        <br>

                        <div class="input-group row">
                            <label for="name" class="col-sm-2 col-form-label">@lang('admin.name') en: </label>
                            <div class="col-sm-6">
                                @error('name_en')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="text" class="form-control" name="name_en" id="name_en"
                                       value="{{ old('name', isset($property) ? $property->name_en : null) }}">
                            </div>
                        </div>
                        <br>

                        <button class="btn btn-success">@lang('admin.form_save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
