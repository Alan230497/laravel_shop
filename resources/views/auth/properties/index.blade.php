@extends('layouts.app')

@section('title', __('admin.title.properties_title'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('admin.title.properties_title')</h1>
                <table class="table">
                    <tbody>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            @lang('admin.name')
                        </th>
                        <th>
                            @lang('order.actions')
                        </th>
                    </tr>
                    @foreach($properties as $property)
                        <tr>
                            <td>{{ $property->id }}</td>
                            <td>{{ $property->__('name') }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <form action="{{ route('properties.destroy', $property) }}" method="POST">
                                        <a class="btn btn-success" type="button"
                                           href="{{ route('properties.show', $property) }}">@lang('order.open')</a>
                                        <a class="btn btn-warning" type="button"
                                           href="{{ route('properties.edit', $property) }}">@lang('admin.edit')</a>
                                        <a class="btn btn-dark" type="button"
                                           href="{{ route('property-options.index', $property) }}">@lang('admin.add_property_variant')</a>
                                        @csrf
                                        @method('DELETE')
                                        <input class="btn btn-danger" type="submit" value="@lang('admin.delete')"></form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                        {{ $properties->links() }}
                <a class="btn btn-success" type="button"
                   href="{{ route('properties.create') }}">@lang('admin.add_property')</a>
            </div>
        </div>
    </div>
@endsection
