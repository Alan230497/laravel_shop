<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@lang('main.online_shop'): @yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <script src="/js/app.js"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="/css/starter-template.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('index') }}">@lang('main.online_shop')</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li @routeactive('index')><a href="{{ route('index') }}">@lang('main.all_products')</a></li>
                <li @routeactive('categor*')><a href="{{ route('categories') }}">@lang('main.categories')</a></li>
                <li @routeactive('basket*')><a href="{{ route('basket') }}">@lang('main.your_cart')</a></li>
                <li><a href="{{ route('reset') }}">@lang('main.reset_project_to_default_state')</a></li>
                <li><a href="{{ route('locale', __('main.set_lang')) }}">@lang('main.set_lang')</a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $currencySymbol }}<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                       @foreach($currencies as $currency)
                            <li><a href="{{ route('currency', $currency->code) }}">{{ $currency->symbol }}</a></li>
                           @endforeach
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
            @auth
                @admin
                    <li><a href="{{ route('home') }}">@lang('admin.admin_panel')</a></li>
                @else
                    <li><a href="{{ route('person.orders.index') }}">@lang('main.my_orders')</a></li>
                    @endadmin
                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        @lang('main.logout')
                        </a></li>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">

                        @csrf
                    </form>
                </ul>
            @endauth
            @guest
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('login') }}">@lang('main.login')</a></li>
                <li><a href="{{ route('register') }}">@lang('main.register')</a></li>
            </ul>
            @endguest
        </div>
    </div>
</nav>
<main class="py-4 content">
    <div class="container content">
        <div class="starter-template">
            @if(session()->has('success'))
                <p class="alert alert-success cc_cursor">{{ session()->get('success') }}</p>
            @endif
                @if(session()->has('warning'))
                    <p class="alert alert-warning cc_cursor">{{ session()->get('warning') }}</p>
            @endif
            @yield('content')
        </div>
    </div>
</main>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6"><p>Категории товаров</p>
                <ul>
                    @foreach($categories as $category)
                        <li><a href="{{ route('category', $category->code) }}">{{ $category->__('name') }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-lg-6"><p>Самые популярные товары</p>
                <ul>
                    @foreach($bestSkus as $bestSku)
                        <li><a href="{{ route('sku', [$bestSku->product->category->code,
                        $bestSku->product->code, $bestSku]) }}">{{ $bestSku->product->__('name') }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</footer>

</body>
</html>

