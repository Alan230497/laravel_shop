<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@admin @lang('admin.admin_title'): @endadmin @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">


                    <!-- Right Side Of Navbar -->
{{--                    <ul class="navbar-nav ml-auto">--}}
                        <ul class="nav navbar-nav">
                            <!-- Authentication Links -->
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('index') }}">@lang('main.back_to_site')</a>
                            </li>
                        @admin
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('categories.index') }}">@lang('admin.title.categories_title')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('products.index') }}">@lang('admin.title.products_title')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('properties.index') }}">@lang('admin.title.properties_title')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('coupons.index') }}">Купоны</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">@lang('admin.title.orders_title')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('merchants.index') }}">Поставщики</a>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item"><a class="nav-link" href="{{ route('locale', __('main.set_lang')) }}">@lang('main.set_lang')</a></li>
                        @endadmin
                        @authnotadmin
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('basket') }}">@lang('admin.title.orders_title')</a>
                        </li>
                        @endauthnotadmin
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Войти') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Регистрация') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   @admin Администратор @else {{ Auth::user()->name }} @endadmin
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        @lang('main.logout')
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
