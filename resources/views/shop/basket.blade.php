@extends('layouts.index')

@section('title', __('main.title.your_cart_title'))

@section('content')
        <h1>@lang('basket.basket_main.basket')</h1>
        <p>@lang('basket.basket_main.order_registration')</p>
        <div class="panel">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>@lang('basket.basket_main.name')</th>
                    <th>@lang('basket.basket_main.number')</th>
                    <th>@lang('basket.basket_main.price')</th>
                    <th>@lang('basket.basket_main.cost')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->skus as $sku)
                    <tr>
                        <td>
                            <a href="{{ route('sku', [$sku->product->category->code, $sku->product->code, $sku]) }}">
                                <img height="56px" src="{{ Storage::url($sku->product->image) }}">
                                {{ $sku->product->__('name') }}
                            </a>
                        </td>
                        <td><span class="badge">{{ $sku->countInOrder }}</span>
                            <div class="btn-group form-inline">
                                <form action="{{ route('basket-remove', $sku) }}" method="POST">
                                    <button type="submit" class="btn btn-danger" href=""><span
                                            class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>
                                    <input type="hidden" name="_token" value="IjfbqQlLKnGo1SXUSozKgJR4QX3wnQzjc9WWgzmB">
                                    @csrf
                                </form>
                                <form action="{{ route('basket-add', $sku) }}" method="POST">
                                    <button type="submit" class="btn btn-success"
                                            href=""><span
                                            class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                    <input type="hidden" name="_token" value="IjfbqQlLKnGo1SXUSozKgJR4QX3wnQzjc9WWgzmB">
                                    @csrf
                                </form>
                            </div>
                        </td>
                        <td>{{ $sku->price }} {{ $currencySymbol }}</td>
                        <td>{{ $sku->price * $sku->countInOrder }} {{ $currencySymbol }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3">@lang('basket.basket_main.total_cost'):</td>
                    @if($order->hasCoupon())
                        <td><strike>{{ $order->getFullSum(false) }}</strike><b> {{ $order->getFullSum() }} </b> {{ $currencySymbol }}</td>
                    @else
                        <td>{{ $order->getFullSum() }} {{ $currencySymbol }}</td>
                    @endif
                </tr>
                </tbody>
            </table>
            <br>
            @if(!$order->hasCoupon())
                <div class="row">
                    <div class="form-inline pull-right">
                        <form method="POST" action="{{ route('set-coupon') }}">
                            @csrf
                            <label for="coupon">Добавить купон:</label>
                            <input class="form-control" type="text" name="coupon">
                            <button type="submit" class="btn btn-success">Применить</button>
                        </form>
                    </div>
                </div>
                @error('coupon')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            <br>
            @else
                <div>Вы используете купон {{ $order->coupon->code }}</div>
            @endif

            <div class="row">
                <div class="btn-group pull-right" role="group">
                    <a type="button" class="btn btn-success" href="{{ route('basket-place') }}">@lang('basket.basket_main.place_an_order')</a>
                </div>
            </div>
        </div>
@endsection
