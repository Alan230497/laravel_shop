@extends('layouts.index')

@section('title', __('main.title.categories_title'))

@section('content')
        @foreach($categories as $category)
            <div class="panel">
                <a href="{{ route('category', $category->code) }}">
                    <img class="max-heightCategory" src="{{ Storage::url($category->image) }}">
                    <h2>{{ $category->__('name') }}</h2>
                </a>
                <p>
                    {{ $category->__('description') }}
                </p>
            </div>
        @endforeach
@endsection
