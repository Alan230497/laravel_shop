@extends('layouts.index')

@section('title', 'Оформить заказ')

@section('content')
        <h1>@lang('order.confirm_order'):</h1>
        <div class="container">
            <div class="row justify-content-center">
                <p>@lang('basket.basket_main.total_cost'): <b>{{ $order->getFullSum() }} {{ $currencySymbol }}</b></p>
                <form action="{{ route('basket-confirm') }}" method="POST">
                    <div>
                        <p>@lang('order.contact_you'):</p>

                        <div class="container">
                            <div class="form-group">
                                <label for="name" class="control-label col-lg-offset-3 col-lg-2">@lang('basket.basket_main.name'): </label>
                                <div class="col-lg-4">
                                    <input type="text" name="name" id="name" value="" class="form-control">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="email" class="control-label col-lg-offset-3 col-lg-2">Email: </label>
                                <div class="col-lg-4">
                                    <input type="text" name="email" id="email" value="" class="form-control">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="phone" class="control-label col-lg-offset-3 col-lg-2">@lang('order.phone_number'): </label>
                                <div class="col-lg-4">
                                    <input type="text" name="phone" id="phone" value="" class="form-control">
                                </div>
                            </div>
                            <br>
                            <br>
                        </div>
                        <br>
                        <input type="hidden" name="_token" value="IjfbqQlLKnGo1SXUSozKgJR4QX3wnQzjc9WWgzmB">
                        @csrf
                        <input type="submit" class="btn btn-success" value="@lang('order.confirm_order_button')">
                    </div>
                </form>
            </div>
        </div>
@endsection
