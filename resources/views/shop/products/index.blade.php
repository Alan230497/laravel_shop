@extends('layouts.index')

@section('title', 'Товар')

@section('content')
    <h1>{{ $sku->product->__('name') }}</h1>
    <h2>{{ $sku->product->category->name }}</h2>
    <p>Цена: <b>{{ $sku->price }} {{ $currencySymbol }}</b></p>

    @isset($sku->product->properties)
        @foreach($sku->propertyOptions as $propertyOption)
            <h4>{{ $propertyOption->property->__('name') }}: {{ $propertyOption->__('name') }}</h4>
        @endforeach
    @endisset

    <img class="max-height" src="{{ Storage::url($sku->product->image) }}">
    <br>
    <p>{{ $sku->product->__('description') }}</p>

    @if($sku->isAvailable())
        <form action="{{ route('basket-add', $sku) }}" method="POST">
            <button type="submit" class="btn btn-success" role="button">Добавить в корзину</button>

            @csrf
        </form>
    @else
        <span>Не доступен</span>
        <br>
        <span>Сообщить мне, когда товар появится в наличии:</span>
        <div class="warning"></div>
        @if($errors->get('email'))
            {!! $errors->get('email')[0] !!}
        @endif
        <form method="post" action="{{ route('subscription', $sku) }}">
            @csrf
            <input type="text" name="email">
            <button type="submit">Отправить</button>
        </form>
    @endif
@endsection
