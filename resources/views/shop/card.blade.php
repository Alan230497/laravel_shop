<div class="col-sm-6 col-md-4">
    <div class="thumbnail">
        <div class="labels">
            @if($sku->product->isNew())
                <span class="badge badge-success">@lang('main.filter_labels.new')</span>
            @endif
            @if($sku->product->isHit())
                <span class="badge badge-warning">@lang('main.filter_labels.hit')</span>
            @endif
            @if($sku->product->isRecommend())
                <span class="badge badge-danger">@lang('main.filter_labels.recommend')</span>
            @endif
        </div>
        <img class="img-height" src="{{ Storage::url($sku->product->image) }}" alt="{{ $sku->product->__('name') }}">
        <div class="caption max-height-product">
            <h3>{{ $sku->product->__('name') }}</h3>
            @isset($sku->product->properties)
                @foreach($sku->propertyOptions as $propertyOption)
                    <h4>{{ $propertyOption->property->__('name') }}: {{ $propertyOption->__('name') }}</h4>
                @endforeach
            @endisset
            <p>{{ $sku->price }} {{ $currencySymbol }}</p>
            <p>
            <form action="{{ route('basket-add', $sku) }}" method="POST">
                @if($sku->isAvailable())
                    <button type="submit" class="btn btn-primary" role="button">@lang('main.your_cart')</button>
                @else
                    @lang('main.not_available')
                @endif
                <a href="{{ route('sku',
                    [isset($category) ? $category->code :
                    $sku->product->category->code, $sku->product->code, $sku->id]) }}"
                    class="btn btn-default"
                    role="button">@lang('main.more_details')</a>
                <input type="hidden" name="_token" value="tG9Zh9pLdxOex0ADB7I2nHOHlaVIFtTTnB6aKSYJ">
                @csrf
            </form>
            </p>
        </div>
    </div>
</div>
