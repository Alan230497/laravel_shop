<?php

namespace App\Http\Controllers\Shop;

use App\Classes\Basket;
use App\Http\Requests\AddCouponRequest;
use App\Models\Coupon;
use App\Models\Sku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BasketController extends BaseController
{
    public function basket()
    {
        $order = (new Basket())->getOrder();

        return view('shop.basket', compact('order'));
    }

    public function basketConfirm(Request $request)
    {
        $basket = new Basket();
        if ($basket->getOrder()->hasCoupon() && !$basket->getOrder()->coupon->availableForUse()) {
            $basket->clearCoupon();
            session()->flash('warning',  'Купон не доступен для использования');
            return redirect()->route('basket');
        }
        $email = Auth::check() ? Auth::user()->email : $request->email;

        if ($basket->saveOrder($request->name, $request->phone, $email)) {
            session()->flash('success', __('basket.you_order_confirmed'));
        } else {
            session()->flash('warning',  __('basket.not_available'));
        }

        return redirect()->route('index');
    }

    public function basketPlace()
    {
        $basket = new Basket();
        $order = $basket->getOrder();

        if (!$basket->countAvailable()) {
            session()->flash('warning', __('basket.not_available'));
            return redirect()->route('basket');
        }

        return view('shop.order', compact('order'));
    }

    public function basketAdd(Sku $sku)
    {
        $result = (new Basket(true))->addSku($sku);

        if ($result) {
            session()->flash('success', __('basket.basket_main.product_added') . ' ' . $sku->product->__('name'));
        } else {
            session()->flash('warning', $sku->product->__('name') . __(' basket.basket_main.product_not_available_more'));
        }

        return redirect()->route('basket');
    }

    public function basketRemove(Sku $sku)
    {
        (new Basket())->removeSku($sku);

        session()->flash('warning', __('basket.basket_main.product_remove') . ' ' . $sku->product->__('name'));

        return redirect()->route('basket');
    }

    public function setCoupon(AddCouponRequest $request)
    {
        $coupon = Coupon::where('code', $request->coupon)->first();
        if ($coupon->availableForUse()) {
            $result = (new Basket())->setCoupon($coupon);
            if ($result) {
                session()->flash('success', 'Купон ' . $coupon->code . ' добавлен к заказу');
            } else {
                session()->flash('warning', $coupon->code . ' не может быть использован');
            }
        }

        return redirect()->route('basket');

    }
}
