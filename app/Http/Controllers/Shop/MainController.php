<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductsFilterRequest;
use App\Http\Requests\SubscriptionRequest;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Sku;
use App\Models\Subscription;
use Debugbar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;


class MainController extends BaseController
{
    public function index(ProductsFilterRequest $request)
    {
        $skusQuery = Sku::with(['product', 'product.category']);

        if ($request->filled('price_from')) {
            $skusQuery->where('price', '>=', $request->price_from);
        }

        if ($request->filled('price_to')) {
            $skusQuery->where('price', '<=', $request->price_to);
        }

        foreach (['new', 'hit', 'recommend'] as $field) {
            if ($request->has($field)) {
                $skusQuery->whereHas('product', function ($query) use ($field) {
                    $query->$field();
                });
            }
        }


        $skus = $skusQuery->paginate(6)->withPath("?" . $request->getQueryString());

        return view('shop.index', compact('skus'));
    }

    public function categories()
    {
        return view('shop.categories.index' );
    }

    public function category($code)
    {
        $category = Category::where('code', $code)->first();
        return view('shop.categories.category', compact('category'));
    }

    public function sku($categoryCode, $productCode, Sku $sku)
    {
        if ($sku->product->code != $productCode) {
            abort(404, 'Product not found');
        }
        if ($sku->product->category->code != $categoryCode) {
            abort(404, 'Category not found');
        }
//        $product = Product::withTrashed()->byCode($productCode)->firstOrFail();
//        $product = Product::where('code', $productCode)->first();

        return view('shop.products.index', compact('sku'));
    }

    public function subscribe(SubscriptionRequest $request, Sku $sku)
    {
        Subscription::create([
            'email' => $request->email,
            'sku_id' => $sku->id,
        ]);

        return redirect()->back()->with('success', __('main.main.subscribe_product'));
    }

    public function changeLocale($locale)
    {
        $availableLocales = ['ru', 'en'];
        if (!in_array($locale, $availableLocales)) {
            $locale = config('app.locale');
        }

        session(['locale' => $locale]);
        App::setLocale($locale);

        return redirect()->back();
    }
    public function changeCurrency($currencyCode)
    {
        $currency = Currency::byCode($currencyCode)->firstOrFail();
//        $availableCurrencies = ['{{ $currencySymbol }}', '$', '€'];
//
//        if (!in_array($currency, $availableCurrencies)) {
//            $currency = '{{ $currencySymbol }}';
//        }

        session(['currency' => $currency->code]);

        return redirect()->back();

    }
}
