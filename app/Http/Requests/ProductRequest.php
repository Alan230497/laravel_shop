<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'          => 'required|min:3|max:255|',
            'name'          => 'required|min:3|max:255|',
            'description'   => 'required|min:5',
        ];
    }

    public function messages()
    {
        return [
            'required'  => 'Поле :attribute обязательно для заполнения',
            'min'       => 'Поле :attribute должно состоять минимум из :min символов',
            'max'       => 'Поле :attribute не должно содержать более :max символов',
        ];
    }
}
